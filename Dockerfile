FROM		debian:stretch

MAINTAINER	rbartl<robert@edv-bartl.at>

ENV		DEBIAN_FRONTEND noninteractive

ENV		http_proxy ""
ENV		https_proxy ""

RUN		apt-get update \
		&& apt-get install -y \
			bash-completion \
			curl \
			lsof \
			net-tools \
			vim \
			inetutils-syslogd \
			postfix \
			libsasl2-modules \
			dropbear \
		&& apt-get clean \
		&& apt-get -y autoremove \
		&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV		CRE_VERSION 1.4.0

RUN		apt-get update \
		&& cd /tmp \
		&& curl -sLO https://mathias-kettner.de/support/"$CRE_VERSION"/check-mk-raw-"$CRE_VERSION"_0.stretch_amd64.deb \
		&& (dpkg -i check-mk-raw-"$CRE_VERSION"_0.stretch_amd64.deb || apt-get -y -f install) \
		&& apt-get clean \
		&& apt-get -y autoremove \
		&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN		sed -i 's|echo "on"$|echo "off"|' /opt/omd/versions/default/lib/omd/hooks/TMPFS

RUN		echo 'root:root' | chpasswd

RUN		sed -i 's,^#force_color_prompt,force_color_prompt,' /etc/skel/.bashrc && \
		sed -i "s,^#alias ll='ls -l',alias ll='ls -la'," /etc/skel/.bashrc && \
		sed 's,01;32m,01;31m,' /etc/skel/.bashrc > /root/.bashrc && \
		sed -i 's,^"syntax on,if has("syntax")\n  syntax on\nendif,' /etc/vim/vimrc

ENV		TINI_VERSION v0.14.0
ADD		https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN		chmod +x /tini
ENTRYPOINT	["/tini", "--"]

COPY		entrypoint.sh /entrypoint.sh
RUN		chmod +x /entrypoint.sh
CMD		["/entrypoint.sh"]

RUN 		apt-get update && \
                apt-get install -y apt-transport-https gnupg2 && \
                apt-get install -y wget && \
                wget -O - https://packages.pagerduty.com/GPG-KEY-pagerduty | apt-key add - && \
                echo "deb https://packages.pagerduty.com/pdagent deb/" >/etc/apt/sources.list.d/pdagent.list  && \
                apt-get update && \
                apt-get install -y pdagent pdagent-integrations 


RUN 		postconf -e smtp_host_lookup=native,dns

ADD		pagerduty-agent /opt/pagerduty-agent
RUN		cp /opt/pagerduty-agent /opt/omd/versions/${CRE_VERSION}.cre/share/check_mk/notifications

RUN		wget http://software.victorops.com/apt/pool/main/v/victorops-nagios/victorops-nagios_1.4.20_all.deb -O /tmp/victorops-nagios_1.4.20_all.deb && \
		dpkg -i /tmp/victorops-nagios_1.4.20_all.deb && rm /tmp/victorops-nagios_1.4.20_all.deb



